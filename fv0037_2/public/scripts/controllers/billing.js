'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:BillingCtrl
 * @description
 * # BillingCtrl
 * Controller of the minovateApp
 */
app.controller('BillingCtrl', function($scope, $http) {
    $scope.page = {
        title: 'Billing',
        subtitle: 'This is the billing view.'
    };
    $scope.munnies = 5407;
    $scope.unBindCall = $scope.$on('itemDropped', function(e, id) {
        console.log("billing scope test: " + $scope.munnies + " with id: " + id);

        var Querydata = {
            id: id,
            value: "billing scope test: "  + $scope.munnies,
            name: "",
            mail: "",
            telephone: "",
            address: "",
            information: ""
        };

        $http({
            method: 'POST',
            url: '/api',
            data: Querydata,
        }).then(function successCallback(response) {
            if(response.status == 201)
            {
                console.log("Object Sucessfully added to MongoDB -> " + JSON.stringify(response.body));
            }
            else {
                console.error(response);
            }

        }, function errorCallback(response) {
            console.error(response);
        });

        $scope.unBindCall();
    });

    $scope.$on('$destroy', function () {
        $scope.unBindCall();
    });

    $scope.init = function() {
        //console.log("Initialized !");
    }
});
