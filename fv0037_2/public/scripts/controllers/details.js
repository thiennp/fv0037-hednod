'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:DetailsController
 * @description
 * # DetailsController
 * Controller of the minovateApp
 */
app
  .controller('DetailsController', function($scope, $http, $stateParams) {
    $scope.itemDetails = {};
    $scope.init = function() {
      console.log("Item Id is: " + $stateParams.itemId);
      $http({
          method: 'GET',
          url: '/api/' + $stateParams.itemId,
        })
        .then(function successCallback(response) {
          if (response.status == 201) {
            $scope.itemDetails = response.data;
            console.log("Sucessfully Read Data from MongoDB...." + JSON.stringify(response.data));
          } else {
            console.error(response);
          }
        }, function errorCallback(response) {
          console.error(response);
        });
    };

    $scope.checkInformationVisibility = function(name) {
      if (name == "") {
        return true;
      } else {
        return false;
      }
    };
  });
