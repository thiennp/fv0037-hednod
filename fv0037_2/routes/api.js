var express = require('express');
var node = require('../model/node');
var router = express.Router();


/* GET Read single / all nodes */
router.get('/', function(req, res, next) {

        node.find(function(err, result){
            if(err) {
                console.error("Error Occurred when Inserting Product...",err);
                res.status(401);
                res.json({result: "Error: " + JSON.stringify(err)});
                return;
            }
            res.status(201);
            res.json(result);
        });


});

router.get('/:id', function(req, res, next) {
    node.findOne({id: req.params.id},function(err, result){
        if(err) {
            console.error("Error Occurred when Inserting Product...",err);
            res.status(401);
            res.json({result: "Error: " + JSON.stringify(err)});
            return;
        }
        res.status(201);
        res.json(result);
    });
});



/* POST Add / Update new node */
router.post('/', function(req, res, next) {

    if(req.body.id == null) {
        res.status(401);
        res.json({result: "Error: Id cannot be NULL"});
        return;
    }

    node.update({id: req.body.id}, req.body, {upsert: true, setDefaultsOnInsert: true}, function(err){
        if(err) {
            console.error("Error Occurred when Inserting Product...",err);
            res.status(401);
            res.json({result: "Error: " + JSON.stringify(err)});
            return;
        }
        res.status(201);
        res.json({result: "Node Added / Updated Successfully"})
    });

});



/* DELETE delete node from dashboard */
router.delete('/:id', function(req, res, next) {

    node.findOneAndRemove( { id: req.params.id },function(err, result){
        if(err) {
            console.error("Error Occurred when Deleting Product...",err);
            res.status(401);
            res.json({result: "Error: " + JSON.stringify(err)});
            return;
        }
        res.status(201);
        res.json({result: "Node Removed Successfully"});
    });

});



module.exports = router;
