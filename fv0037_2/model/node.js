var mongoose = require('mongoose');

var NodeSchema = new mongoose.Schema({
    id: { type: Number, required: true, unique: true},
    value: {type: String, default: ''},
    name: {type: String, default: ''},
    mail: {type: String, default: ''},
    telephone: {type: String, default: ''},
    address: {type: String, default: ''},
    information: {type: String, default: ''},
    updated_at: { type: Date, default: Date.now }
});

module.exports = mongoose.model('nodes', NodeSchema);