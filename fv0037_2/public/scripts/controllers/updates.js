'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:UpdatesController
 * @description
 * # UpdatesController
 * Controller of the minovateApp
 */
app
  .controller('UpdatesController', function($scope, $http, $stateParams, $state) {
    $scope.itemDetails = {};

    $scope.init = function() {
      console.log("Item Id is: " + $stateParams.itemId);
      $http({
        method: 'GET',
        url: '/api/' + $stateParams.itemId,
      }).then(function successCallback(response) {
        if (response.status == 201) {
          $scope.itemDetails = response.data;
          console.log("Sucessfully Read Data from MongoDB...." + JSON.stringify(response.data));
        } else {
          console.error(response);
        }
      }, function errorCallback(response) {
        console.error(response);
      });
    };

    $scope.updateDetails = function() {
      $http({
          method: 'POST',
          url: '/api',
          data: $scope.itemDetails,
        })
        .then(function successCallback(response) {
          if (response.status == 201) {
            console.log("Object Successfully updated -> " + JSON.stringify(response.body));
          } else {
            console.error(response);
          }
        }, function errorCallback(response) {
          console.error(response);
        });

      $state.go('app.dashboard');
    };
  });
