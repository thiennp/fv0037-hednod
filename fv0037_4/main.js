
var map;

function initialize() {
  var styles = [{
    "featureType": "all",
    "elementType": "labels.text.fill",
    "stylers": [{
      "saturation": 36
    }, {
        "color": "#22252e"
      }, {
        "lightness": 40
      }]
  }, {
      "featureType": "all",
      "elementType": "labels.text.stroke",
      "stylers": [{
        "visibility": "on"
      }, {
          "color": "#22252e"
        }, {
          "lightness": 16
        }]
    }, {
      "featureType": "all",
      "elementType": "labels.icon",
      "stylers": [{
        "visibility": "off"
      }]
    }, {
      "featureType": "administrative",
      "elementType": "geometry.fill",
      "stylers": [{
        "color": "#22252e"
      }, {
          "lightness": 20
        }]
    }, {
      "featureType": "administrative",
      "elementType": "geometry.stroke",
      "stylers": [{
        "color": "#22252e"
      }, {
          "lightness": 17
        }, {
          "weight": 1.2
        }]
    }, {
      "featureType": "administrative",
      "elementType": "labels",
      "stylers": [{
        "visibility": "off"
      }]
    }, {
      "featureType": "administrative.country",
      "elementType": "all",
      "stylers": [{
        "visibility": "simplified"
      }]
    }, {
      "featureType": "administrative.country",
      "elementType": "geometry",
      "stylers": [{
        "visibility": "simplified"
      }]
    }, {
      "featureType": "administrative.country",
      "elementType": "labels.text",
      "stylers": [{
        "visibility": "simplified"
      }]
    }, {
      "featureType": "administrative.province",
      "elementType": "all",
      "stylers": [{
        "visibility": "off"
      }]
    }, {
      "featureType": "administrative.locality",
      "elementType": "all",
      "stylers": [{
        "visibility": "simplified"
      }, {
          "saturation": "-100"
        }, {
          "lightness": "30"
        }]
    }, {
      "featureType": "administrative.neighborhood",
      "elementType": "all",
      "stylers": [{
        "visibility": "off"
      }]
    }, {
      "featureType": "administrative.land_parcel",
      "elementType": "all",
      "stylers": [{
        "visibility": "off"
      }]
    }, {
      "featureType": "landscape",
      "elementType": "all",
      "stylers": [{
        "visibility": "simplified"
      }, {
          "gamma": "0.00"
        }, {
          "lightness": "74"
        }]
    }, {
      "featureType": "landscape",
      "elementType": "geometry",
      "stylers": [{
        "color": "#22252e"
      }, {
          "lightness": 20
        }]
    }, {
      "featureType": "landscape.man_made",
      "elementType": "all",
      "stylers": [{
        "lightness": "3"
      }]
    }, {
      "featureType": "poi",
      "elementType": "all",
      "stylers": [{
        "visibility": "off"
      }]
    }, {
      "featureType": "poi",
      "elementType": "geometry",
      "stylers": [{
        "color": "#22252e"
      }, {
          "lightness": 21
        }]
    }, {
      "featureType": "road",
      "elementType": "geometry",
      "stylers": [{
        "visibility": "simplified"
      }]
    }, {
      "featureType": "road.highway",
      "elementType": "geometry.fill",
      "stylers": [{
        "color": "#22252e"
      }, {
          "lightness": 17
        }]
    }, {
      "featureType": "road.highway",
      "elementType": "geometry.stroke",
      "stylers": [{
        "color": "#22252e"
      }, {
          "lightness": 29
        }, {
          "weight": 0.2
        }]
    }, {
      "featureType": "road.arterial",
      "elementType": "geometry",
      "stylers": [{
        "color": "#22252e"
      }, {
          "lightness": 18
        }]
    }, {
      "featureType": "road.local",
      "elementType": "geometry",
      "stylers": [{
        "color": "#22252e"
      }, {
          "lightness": 16
        }]
    }, {
      "featureType": "transit",
      "elementType": "geometry",
      "stylers": [{
        "color": "#22252e"
      }, {
          "lightness": 19
        }]
    }, {
      "featureType": "water",
      "elementType": "geometry",
      "stylers": [{
        "color": "#22252e"
      }, {
          "lightness": 17
        }]
    }];

  // Create a new StyledMapType object, passing it the array of styles,
  // as well as the name to be displayed on the map type control.
  var styledMap = new google.maps.StyledMapType(styles,
    { name: "Styled Map" });

  var latlng = new google.maps.LatLng(15.529279, 73.903656);
  var myOptions = {
    zoom: 11,
    center: latlng,
    mapTypeControlOptions: {
      mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
    },
    disableDefaultUI: true
  };

  map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
  google.maps.event.addListener(map, "click", function (event) {
    $('#sidebar').removeClass('active');
  });
  //Associate the styled map with the MapTypeId and set it to display.
  map.mapTypes.set('map_style', styledMap);
  map.setMapTypeId('map_style');
  setMarkers(map, locations);
}

var locations = [
  [{
    title: 'Location 1',
    description: [
      {
        title: 'Loc 1 - Description 1',
        params: [
          {
            title: 'Param 1-1',
            value: 'Value 1-1'
          },
          {
            title: 'Param 1-2',
            value: 'Value 1-2'
          }
        ]
      },
      {
        title: 'Loc 1 - Description 2',
        params: [
          {
            title: 'Param 2-1',
            value: 'Value 2-1'
          },
          {
            title: 'Param 2-2',
            value: 'Value 2-2'
          },
          {
            title: 'Param 2-3',
            value: 'Value 2-3'
          }
        ]
      },
      {
        title: 'Loc 1 - Description 3',
        params: [
          {
            title: 'Param 3-1',
            value: 'Value 3-1'
          }
        ]
      }
    ]
  }, 15.659627, 73.800792, 4],
  [{
    title: 'Location 2',
    description: [
      {
        title: 'Loc 2 - Description 1',
        params: [
          {
            title: 'Param 1-1',
            value: 'Value 1-1'
          },
          {
            title: 'Param 1-2',
            value: 'Value 1-2'
          },
          {
            title: 'Param 1-3',
            value: 'Value 1-3'
          }
        ]
      },
      {
        title: 'Loc 2 - Description 2',
        params: [
          {
            title: 'Param 2-1',
            value: 'Value 2-1'
          }
        ]
      },
      {
        title: 'Loc 2 - Description 3',
        params: [
          {
            title: 'Param 3-1',
            value: 'Value 3-1'
          },
          {
            title: 'Param 3-2',
            value: 'Value 3-2'
          }
        ]
      }
    ]
  }, 15.547141, 73.755341, 5],
  [{
    title: 'Location 3',
    description: [
      {
        title: 'Loc 3 - Description 1',
        params: [
          {
            title: 'Param 1-1',
            value: 'Value 1-1'
          }
        ]
      },
      {
        title: 'Loc 3 - Description 2',
        params: [
          {
            title: 'Param 2-1',
            value: 'Value 2-1'
          },
          {
            title: 'Param 2-2',
            value: 'Value 2-2'
          }
        ]
      },
      {
        title: 'Loc 3 - Description 3',
        params: [
          {
            title: 'Param 3-1',
            value: 'Value 3-1'
          },
          {
            title: 'Param 3-2',
            value: 'Value 3-2'
          },
          {
            title: 'Param 3-3',
            value: 'Value 3-3'
          }
        ]
      }
    ]
  }, 15.423656, 73.974137, 3]
];

var markers = [];

function setMarkers(map, locations) {
  var image = 'https://upload.wikimedia.org/wikipedia/commons/5/59/Empty.png';
  for (var i = 0; i < locations.length; i++) {
    var beach = locations[i];
    var myLatLng = new google.maps.LatLng(beach[1], beach[2]);
    var marker = new google.maps.Marker(
      {
        position: myLatLng,
        map: map,
        icon: image,
        title: beach[0].title,
        description: beach[0].description,
        zIndex: beach[3]
      });
    markers[i] = marker;
    setIcon(beach, i);
  }
}

function setIcon(beach, index) {
  setTimeout(function () {
    var currentLatLng = new google.maps.LatLng(beach[1], beach[2]);
    var iconPosition = getCanvasXY(currentLatLng);

    var x = iconPosition.x;
    var y = iconPosition.y;

    var projection;
    var contextmenuDir;
    var cls = Math.floor(Math.random() * 1000000);
    projection = map.getProjection();
    contextmenuDir = document.createElement("div");
    contextmenuDir.className = 'mapicon mapicon-' + cls;
    contextmenuDir.innerHTML = '<div class="click-area"></div>';
    $(map.getDiv()).append(contextmenuDir);
    var mapWidth = $('#map_canvas').width();
    var mapHeight = $('#map_canvas').height();

    $('.mapicon-' + cls).css('left', x);
    $('.mapicon-' + cls).css('top', y);
    $('.mapicon-' + cls + ' .click-area').click(function () {
      showSidebar(markers[index]);
    });
  }, 400);
}

function getCanvasXY(currentLatLng) {
  var scale = Math.pow(2, map.getZoom());
  var nw = new google.maps.LatLng(
    map.getBounds().getNorthEast().lat(),
    map.getBounds().getSouthWest().lng()
  );
  var worldCoordinateNW = map.getProjection().fromLatLngToPoint(nw);
  var worldCoordinate = map.getProjection().fromLatLngToPoint(currentLatLng);
  var currentLatLngOffset = new google.maps.Point(
    Math.floor((worldCoordinate.x - worldCoordinateNW.x) * scale),
    Math.floor((worldCoordinate.y - worldCoordinateNW.y) * scale)
  );
  return currentLatLngOffset;
}

function setMenuXY(currentLatLng) {
  var mapWidth = $('#map_canvas').width();
  var mapHeight = $('#map_canvas').height();
  var menuWidth = $('.contextmenu').width();
  var menuHeight = $('.contextmenu').height();
  var clickedPosition = getCanvasXY(currentLatLng);
  var x = clickedPosition.x;
  var y = clickedPosition.y;

  if ((mapWidth - x) < menuWidth)
    x = x - menuWidth;
  if ((mapHeight - y) < menuHeight)
    y = y - menuHeight;

  $('.contextmenu').css('left', x);
  $('.contextmenu').css('top', y);
}

function contextMenuHtml(title, description) {
  var html = '';
  html += '<div class="box-title">';
  html += '  <div>' + title + '</div>';
  html += '  <div class="box-closeButton">';
  html += '    <svg viewBox="0 0 24 24">';
  html += '      <path d="M22.2,4c0,0,0.5,0.6,0,1.1l-6.8,6.8l6.9,6.9c0.5,0.5,0,1.1,0,1.1L20,22.3c0,0-0.6,0.5-1.1,0L12,15.4l-6.9,6.9c-0.5,0.5-1.1,0-1.1,0L1.7,20c0,0-0.5-0.6,0-1.1L8.6,12L1.7,5.1C1.2,4.6,1.7,4,1.7,4L4,1.7c0,0,0.6-0.5,1.1,0L12,8.5l6.8-6.8c0.5-0.5,1.1,0,1.1,0L22.2,4z">';
  html += '      </path>';
  html += '    </svg>';
  html += '  </div>';
  html += '</div>';
  for (var i = 0; i < description.length; i++) {
    html += '<h3>' + description[i].title + '</h3>';
    for (var j = 0; j < description[i].params.length; j++) {
      html += '<div class="description-title col-xs-6">' + description[i].params[j].title + '</div>';
      html += '<div class="description-value col-xs-6">' + description[i].params[j].value + '</div>';
      html += '<div class="clearfix"></div>';
    }
  }

  return html;
}

function showSidebar(marker) {
  $('#sidebar').addClass('active');
  $('#sidebar').html(contextMenuHtml(marker.title, marker.description));
  $('.box-closeButton').click(function () {
    $('#sidebar').removeClass('active');
  });
}

$(document).ready(function () {
  initialize();
});