function markerHTML(i) {
  var html = '';
  html += '<div class="load">';
  html += '  <div class="load-pulse">';
  html += '    <div class="load-pulse-container"></div>';
  html += '  </div>';
  html += '  <nav class="menu menu-1">';
  html += '    <input type="checkbox" href="#" class="menu-open-' + i + ' menu-open" name="menu-open-' + i + '" id="menu-open-' + i + '" />';
  html += '    <label class="menu-open-button" for="menu-open-' + i + '">';
  html += '      <span class="hamburger hamburger-1"></span>';
  html += '      <span class="hamburger hamburger-2"></span>';
  html += '      <span class="hamburger hamburger-3"></span>';
  html += '    </label>';

  html += '    <a href="#" class="menu-item"> <i class=""></i> </a>';
  html += '    <a href="#" class="menu-item"> <i class=""></i> </a>';
  html += '    <a href="#" class="menu-item"> <i class=""></i> </a>';
  html += '    <a href="#" class="menu-item"> <i class=""></i> </a>';

  html += '    <svg xmlns="http://www.w3.org/2000/svg" version="1.1">';
  html += '      <defs>';
  html += '        <filter id="shadowed-goo">';

  html += '          <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="10" />';
  html += '          <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo" />';
  html += '          <feGaussianBlur in="goo" stdDeviation="3" result="shadow" />';
  html += '          <feColorMatrix in="shadow" mode="matrix" values="0 0 0 0 0  0 0 0 0 0  0 0 0 0 0  0 0 0 1 -0.2" result="shadow" />';
  html += '          <feOffset in="shadow" dx="1" dy="1" result="shadow" />';
  html += '          <feComposite in2="shadow" in="goo" result="goo" />';
  html += '          <feComposite in2="goo" in="SourceGraphic" result="mix" />';
  html += '        </filter>';
  html += '        <filter id="goo">';
  html += '          <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="10" />';
  html += '          <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo" />';
  html += '          <feComposite in2="goo" in="SourceGraphic" result="mix" />';
  html += '        </filter>';
  html += '      </defs>';
  html += '    </svg>';
  html += '  </nav>';
  html += '</div>';
  return html;
}


$(document).ready(function () {

    CustomMarker.prototype = new google.maps.OverlayView();
    function CustomMarker(opts) {
        this.setValues(opts);

    }


    CustomMarker.prototype.draw = function () {
        var self = this;
        var div = this.div;
        if (!div) {

            div = this.div = $(markerHTML(1))[0];
            this.pinWrap = this.div.getElementsByClassName('wrap');
            div.style.position = 'absolute';
            div.style.cursor = 'pointer';
            var panes = this.getPanes();
            panes.overlayImage.appendChild(div);

        }
        var point = this.getProjection().fromLatLngToDivPixel(this.position);
        if (point) {
            div.style.left = point.x + 'px';
            div.style.top = point.y + 'px';
        }
    };
    CustomMarker.prototype.animateDrop = function () {
        dynamics.stop(this.pinWrap);
        dynamics.css(this.pinWrap, {
            'transform': 'scaleY(2) translateY(-' + $('#map').outerHeight() + 'px)',
            'opacity': '1',
        });
        dynamics.animate(this.pinWrap, {translateY: 0, scaleY: 1.0,}, {type: dynamics.gravity, duration: 1800,});
        dynamics.stop(this.pin);
        dynamics.css(this.pin, {'transform': 'none',});
        dynamics.animate(this.pin, {scaleY: 0.8}, {type: dynamics.bounce, duration: 1800, bounciness: 600,})
        dynamics.stop(this.pinShadow);
        dynamics.css(this.pinShadow, {'transform': 'scale(0,0)',});
        dynamics.animate(this.pinShadow, {scale: 1,}, {type: dynamics.gravity, duration: 1800,});
    }
    CustomMarker.prototype.animateBounce = function () {
        dynamics.stop(this.pinWrap);
        dynamics.css(this.pinWrap, {'transform': 'load2',});
        dynamics.animate(this.pinWrap, {translateY: -30}, {
            type: dynamics.forceWithGravity,
            bounciness: 0,
            duration: 500,
            delay: 150,
        });
        dynamics.stop(this.pin);
        dynamics.css(this.pin, {'transform': '#load2',});
        dynamics.animate(this.pin, {scaleY: 0.8}, {type: dynamics.bounce, duration: 800, bounciness: 0,});
        dynamics.animate(this.pin, {scaleY: 0.8}, {type: dynamics.bounce, duration: 800, bounciness: 600, delay: 650,});
        dynamics.stop(this.pinShadow);
        dynamics.css(this.pinShadow, {'transform': 'none',});
        dynamics.animate(this.pinShadow, {scale: 0.6,}, {
            type: dynamics.forceWithGravity,
            bounciness: 0,
            duration: 500,
            delay: 150,
        });
    }
    CustomMarker.prototype.animateWobble = function () {
        dynamics.stop(this.pinWrap);
        dynamics.css(this.pinWrap, {'transform': 'none',});
        dynamics.animate(this.pinWrap, {rotateZ: -45,}, {type: dynamics.bounce, duration: 1800,});
        dynamics.stop(this.pin);
        dynamics.css(this.pin, {'transform': 'none',});
        dynamics.animate(this.pin, {scaleX: 0.8}, {type: dynamics.bounce, duration: 800, bounciness: 1800,});
    }




    $(function () {

        var pos = new google.maps.LatLng(52.887726,-2.040472);
        var map = new google.maps.Map(document.getElementById('map'), {zoom: 17, center: pos, disableDefaultUI: true,});
        var marker = new CustomMarker({position: pos, map: map,});


    });

});