mapboxgl.accessToken = 'pk.eyJ1IjoiYnViYnoiLCJhIjoiY2lmMWJ1anQyMGJnc3NkbTVzYjY4eG16MyJ9.zQ7ewyZliRhCzqroFjxRsw';

var locations = [
    {
        title: 'Location 1',
        description: [
            {
                title: 'Loc 1 - Description 1',
                params: [
                    {
                        title: 'Param 1-1',
                        value: 'Value 1-1'
                    },
                    {
                        title: 'Param 1-2',
                        value: 'Value 1-2'
                    }
                ]
            },
            {
                title: 'Loc 1 - Description 2',
                params: [
                    {
                        title: 'Param 2-1',
                        value: 'Value 2-1'
                    },
                    {
                        title: 'Param 2-2',
                        value: 'Value 2-2'
                    },
                    {
                        title: 'Param 2-3',
                        value: 'Value 2-3'
                    }
                ]
            },
            {
                title: 'Loc 1 - Description 3',
                params: [
                    {
                        title: 'Param 3-1',
                        value: 'Value 3-1'
                    }
                ]
            }
        ]
    },
    {
        title: 'Location 2',
        description: [
            {
                title: 'Loc 2 - Description 1',
                params: [
                    {
                        title: 'Param 1-1',
                        value: 'Value 1-1'
                    },
                    {
                        title: 'Param 1-2',
                        value: 'Value 1-2'
                    },
                    {
                        title: 'Param 1-3',
                        value: 'Value 1-3'
                    }
                ]
            },
            {
                title: 'Loc 2 - Description 2',
                params: [
                    {
                        title: 'Param 2-1',
                        value: 'Value 2-1'
                    }
                ]
            },
            {
                title: 'Loc 2 - Description 3',
                params: [
                    {
                        title: 'Param 3-1',
                        value: 'Value 3-1'
                    },
                    {
                        title: 'Param 3-2',
                        value: 'Value 3-2'
                    }
                ]
            }
        ]
    },
    {
        title: 'Location 3',
        description: [
            {
                title: 'Loc 3 - Description 1',
                params: [
                    {
                        title: 'Param 1-1',
                        value: 'Value 1-1'
                    }
                ]
            },
            {
                title: 'Loc 3 - Description 2',
                params: [
                    {
                        title: 'Param 2-1',
                        value: 'Value 2-1'
                    },
                    {
                        title: 'Param 2-2',
                        value: 'Value 2-2'
                    }
                ]
            },
            {
                title: 'Loc 3 - Description 3',
                params: [
                    {
                        title: 'Param 3-1',
                        value: 'Value 3-1'
                    },
                    {
                        title: 'Param 3-2',
                        value: 'Value 3-2'
                    },
                    {
                        title: 'Param 3-3',
                        value: 'Value 3-3'
                    }
                ]
            }
        ]
    },
    {
        title: 'Location 4',
        description: [
            {
                title: 'Loc 4 - Description 1',
                params: [
                    {
                        title: 'Param 1-1',
                        value: 'Value 1-1'
                    }
                ]
            },
            {
                title: 'Loc 4 - Description 2',
                params: [
                    {
                        title: 'Param 2-1',
                        value: 'Value 2-1'
                    },
                    {
                        title: 'Param 2-2',
                        value: 'Value 2-2'
                    },
                    {
                        title: 'Param 2-3',
                        value: 'Value 2-3'
                    }
                ]
            },
            {
                title: 'Loc 4 - Description 3',
                params: [
                    {
                        title: 'Param 3-1',
                        value: 'Value 3-1'
                    },
                    {
                        title: 'Param 3-2',
                        value: 'Value 3-2'
                    }
                ]
            }
        ]
    }
];

var styles = [
    {
        "featureType": "all",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            }, {
                "color": "#22252e"
            }, {
                "lightness": 40
            }
        ]
    }, {
        "featureType": "all",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            }, {
                "color": "#22252e"
            }, {
                "lightness": 16
            }
        ]
    }, {
        "featureType": "all",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    }, {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#22252e"
            }, {
                "lightness": 20
            }
        ]
    }, {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#22252e"
            }, {
                "lightness": 17
            }, {
                "weight": 1.2
            }
        ]
    }, {
        "featureType": "administrative",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    }, {
        "featureType": "administrative.country",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    }, {
        "featureType": "administrative.country",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    }, {
        "featureType": "administrative.country",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    }, {
        "featureType": "administrative.province",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    }, {
        "featureType": "administrative.locality",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }, {
                "saturation": "-100"
            }, {
                "lightness": "30"
            }
        ]
    }, {
        "featureType": "administrative.neighborhood",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    }, {
        "featureType": "administrative.land_parcel",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    }, {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }, {
                "gamma": "0.00"
            }, {
                "lightness": "74"
            }
        ]
    }, {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#22252e"
            }, {
                "lightness": 20
            }
        ]
    }, {
        "featureType": "landscape.man_made",
        "elementType": "all",
        "stylers": [
            {
                "lightness": "3"
            }
        ]
    }, {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    }, {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#22252e"
            }, {
                "lightness": 21
            }
        ]
    }, {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    }, {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#22252e"
            }, {
                "lightness": 17
            }
        ]
    }, {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#22252e"
            }, {
                "lightness": 29
            }, {
                "weight": 0.2
            }
        ]
    }, {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#22252e"
            }, {
                "lightness": 18
            }
        ]
    }, {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#22252e"
            }, {
                "lightness": 16
            }
        ]
    }, {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#22252e"
            }, {
                "lightness": 19
            }
        ]
    }, {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#22252e"
            }, {
                "lightness": 17
            }
        ]
    }
];

var lMap;

var onSwap = false;

$(document).ready(function () {
    function CustomMarker(opts) {
        this.setValues(opts);
    }

    function initCustomMarker() {
        CustomMarker.prototype = new google.maps.OverlayView();

        CustomMarker.prototype.draw = function () {
            var self = this;
            var div = this.div;
            if (!div) {

                div = this.div = $(markerHTML(1))[0];
                this.pinWrap = this.div.getElementsByClassName('wrap');
                div.style.position = 'absolute';
                div.style.cursor = 'pointer';
                var panes = this.getPanes();
                panes.overlayImage.appendChild(div);

            }
            var point = this.getProjection().fromLatLngToDivPixel(this.position);
            if (point) {
                div.style.left = point.x + 'px';
                div.style.top = point.y + 'px';
            }

            $('.menu-item').click(function () {
                $('.menu-item').removeClass('active');
                $(this).addClass('active');
                var index = $(this).index() - 2;
                if (index !== 1) {
                    showSidebar(locations[index]);
                } else {
                    if (!onSwap) {
                        onSwap = true;
                        setTimeout(function () {
                            $('#google-map').toggleClass('turn-off');
                        }, 10);
                        setTimeout(function () {
                            onSwap = false;
                        }, 1200);
                    }
                }
            });
        };

        CustomMarker.prototype.animateDrop = function () {
            dynamics.stop(this.pinWrap);
            dynamics.css(this.pinWrap, {
                'transform': 'scaleY(2) translateY(-' + $('#map').outerHeight() + 'px)',
                'opacity': '1',
            });
            dynamics.animate(this.pinWrap, { translateY: 0, scaleY: 1.0, }, { type: dynamics.gravity, duration: 1800, });
            dynamics.stop(this.pin);
            dynamics.css(this.pin, { 'transform': 'none', });
            dynamics.animate(this.pin, { scaleY: 0.8 }, { type: dynamics.bounce, duration: 1800, bounciness: 600, })
            dynamics.stop(this.pinShadow);
            dynamics.css(this.pinShadow, { 'transform': 'scale(0,0)', });
            dynamics.animate(this.pinShadow, { scale: 1, }, { type: dynamics.gravity, duration: 1800, });
        }

        CustomMarker.prototype.animateBounce = function () {
            dynamics.stop(this.pinWrap);
            dynamics.css(this.pinWrap, { 'transform': 'load2', });
            dynamics.animate(this.pinWrap, { translateY: -30 }, {
                type: dynamics.forceWithGravity,
                bounciness: 0,
                duration: 500,
                delay: 150,
            });
            dynamics.stop(this.pin);
            dynamics.css(this.pin, { 'transform': '#load2', });
            dynamics.animate(this.pin, { scaleY: 0.8 }, { type: dynamics.bounce, duration: 800, bounciness: 0, });
            dynamics.animate(this.pin, { scaleY: 0.8 }, { type: dynamics.bounce, duration: 800, bounciness: 600, delay: 650, });
            dynamics.stop(this.pinShadow);
            dynamics.css(this.pinShadow, { 'transform': 'none', });
            dynamics.animate(this.pinShadow, { scale: 0.6, }, {
                type: dynamics.forceWithGravity,
                bounciness: 0,
                duration: 500,
                delay: 150,
            });
        }

        CustomMarker.prototype.animateWobble = function () {
            dynamics.stop(this.pinWrap);
            dynamics.css(this.pinWrap, { 'transform': 'none', });
            dynamics.animate(this.pinWrap, { rotateZ: -45, }, { type: dynamics.bounce, duration: 1800, });
            dynamics.stop(this.pin);
            dynamics.css(this.pin, { 'transform': 'none', });
            dynamics.animate(this.pin, { scaleX: 0.8 }, { type: dynamics.bounce, duration: 800, bounciness: 1800, });
        }
    }

    initCustomMarker();

    var lat = 52.887726, lon = -2.040472, zoom = 17, zoomScale = 0.9286;

    function initGoogleMap() {
        var pos = new google.maps.LatLng(lat, lon);
        var styledMap = new google.maps.StyledMapType(styles,
            { name: "Styled Map" });
        var map = new google.maps.Map(document.getElementById('google-map'), {
            zoom: zoom,
            center: pos,
            disableDefaultUI: true,
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
            }
        });

        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');
        var marker = new CustomMarker({
            position: pos, map: map
        });

        map.addListener('center_changed', function () {
            setViewLMap(map);
        });

        map.addListener('zoom_changed', function () {
            setViewLMap(map);
        });
    }

    initGoogleMap();

    function setViewLMap(map) {
        setTimeout(function () {
            center = map.getCenter();
            zoom = map.getZoom();
            lat = center.lat();
            lon = center.lng();
            lMap.setCenter([lon, lat]);
            lMap.setZoom(zoom * zoomScale);
        }, 10);
    }

    function initLMap() {
        lMap = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/bubbz/ciritpsup0001hembuf07v1b0',
            center: [lon, lat],
            zoom: zoom * zoomScale
        });
    }

    initLMap();

    function markerHTML(i) {
        var html = '';
        html += '<div class="load">';
        html += '  <div class="load-pulse">';
        html += '    <div class="load-pulse-container"></div>';
        html += '  </div>';
        html += '  <nav class="menu menu-1">';
        html += '    <input type="checkbox" href="#" class="menu-open-' + i + ' menu-open" name="menu-open-' + i + '" id="menu-open-' + i + '" />';
        html += '    <label class="menu-open-button" for="menu-open-' + i + '">';
        html += '      <span class="hamburger hamburger-1"></span>';
        html += '      <span class="hamburger hamburger-2"></span>';
        html += '      <span class="hamburger hamburger-3"></span>';
        html += '    </label>';

        html += '    <a href="#" class="menu-item"> <i class=""></i> </a>';
        html += '    <a href="#" class="menu-item"> <i class=""></i> </a>';
        html += '    <a href="#" class="menu-item"> <i class=""></i> </a>';
        html += '    <a href="#" class="menu-item"> <i class=""></i> </a>';

        html += '    <svg xmlns="http://www.w3.org/2000/svg" version="1.1">';
        html += '      <defs>';
        html += '        <filter id="shadowed-goo">';

        html += '          <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="10" />';
        html += '          <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo" />';
        html += '          <feGaussianBlur in="goo" stdDeviation="3" result="shadow" />';
        html += '          <feColorMatrix in="shadow" mode="matrix" values="0 0 0 0 0  0 0 0 0 0  0 0 0 0 0  0 0 0 1 -0.2" result="shadow" />';
        html += '          <feOffset in="shadow" dx="1" dy="1" result="shadow" />';
        html += '          <feComposite in2="shadow" in="goo" result="goo" />';
        html += '          <feComposite in2="goo" in="SourceGraphic" result="mix" />';
        html += '        </filter>';
        html += '        <filter id="goo">';
        html += '          <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="10" />';
        html += '          <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo" />';
        html += '          <feComposite in2="goo" in="SourceGraphic" result="mix" />';
        html += '        </filter>';
        html += '      </defs>';
        html += '    </svg>';
        html += '  </nav>';
        html += '</div>';
        return html;
    }

    function showSidebar(location) {
        $('#sidebar').addClass('active');
        $('#sidebar').html(contextMenuHtml(location.title, location.description));
        $('.box-closeButton').click(function () {
            $('#sidebar').removeClass('active');
        });
    }

    function contextMenuHtml(title, description) {
        var html = '';
        html += '<div class="box-title">';
        html += '  <div>' + title + '</div>';
        html += '  <div class="box-closeButton">';
        html += '    <svg viewBox="0 0 24 24">';
        html += '      <path d="M22.2,4c0,0,0.5,0.6,0,1.1l-6.8,6.8l6.9,6.9c0.5,0.5,0,1.1,0,1.1L20,22.3c0,0-0.6,0.5-1.1,0L12,15.4l-6.9,6.9c-0.5,0.5-1.1,0-1.1,0L1.7,20c0,0-0.5-0.6,0-1.1L8.6,12L1.7,5.1C1.2,4.6,1.7,4,1.7,4L4,1.7c0,0,0.6-0.5,1.1,0L12,8.5l6.8-6.8c0.5-0.5,1.1,0,1.1,0L22.2,4z">';
        html += '      </path>';
        html += '    </svg>';
        html += '  </div>';
        html += '</div>';
        for (var i = 0; i < description.length; i++) {
            html += '<h3>' + description[i].title + '</h3>';
            for (var j = 0; j < description[i].params.length; j++) {
                html += '<div class="description-title col-xs-6">' + description[i].params[j].title + '</div>';
                html += '<div class="description-value col-xs-6">' + description[i].params[j].value + '</div>';
                html += '<div class="clearfix"></div>';
            }
        }

        return html;
    }
});