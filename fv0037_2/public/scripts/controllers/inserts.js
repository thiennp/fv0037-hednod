'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:InsertsCtrl
 * @description
 * # InsertsCtrl
 * Controller of the minovateApp
 */
app.controller('InsertsCtrl', function($scope, $http) {
    $scope.page = {
        title: 'Inserts'
    };
    $scope.result = [];
    $scope.init = function() {

        $http({
            method: 'GET',
            url: '/api',
        }).then(function successCallback(response) {
            if(response.status == 201)
            {
                $scope.result = response.data;
                console.log("Sucessfully Read Data from MongoDB...." + JSON.stringify($scope.result));
            }
            else {
                console.error(response);
            }

        }, function errorCallback(response) {
            console.error(response);
        });

    }
});
