'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:CustomersCtrl
 * @description
 * # CustomersCtrl
 * Controller of the minovateApp
 */
app.controller('CustomersCtrl', function($scope, $http) {

    $scope.page = {
        title: 'Customers',
        subtitle: 'Your customizable dashboard.'
    };

    var vm = this;

    vm.munnies = 3434;

    $scope.unBindCall = $scope.$on('itemDropped', function(e, id) {
        console.log("customer scope test: " + vm.munnies + "with id: " + id);

        //var Querydata = { id: id, value: "customer scope test: "  + vm.munnies };
        var Querydata = {
            id: id,
            value: "customer scope test: "  + vm.munnies,
            name: "",
            mail: "",
            telephone: "",
            address: "",
            information: ""
        };

        $http({
            method: 'POST',
            url: '/api',
            data: Querydata,
        }).then(function successCallback(response) {
            if(response.status == 201)
            {
                console.log("Object Sucessfully added to MongoDB -> " + JSON.stringify(response.body));
            }
            else {
                console.error(response);
            }

        }, function errorCallback(response) {
            console.error(response);
        });

        $scope.unBindCall();
    });

    $scope.$on('$destroy', function () {
        $scope.unBindCall();
    });

    $scope.init = function() {
        //console.log("Initialized !");
    };

});
