


var locations = [
  {
    title: 'Location 1',
    description: [
      {
        title: 'Loc 1 - Description 1',
        params: [
          {
            title: 'Param 1-1',
            value: 'Value 1-1'
          },
          {
            title: 'Param 1-2',
            value: 'Value 1-2'
          }
        ]
      },
      {
        title: 'Loc 1 - Description 2',
        params: [
          {
            title: 'Param 2-1',
            value: 'Value 2-1'
          },
          {
            title: 'Param 2-2',
            value: 'Value 2-2'
          },
          {
            title: 'Param 2-3',
            value: 'Value 2-3'
          }
        ]
      },
      {
        title: 'Loc 1 - Description 3',
        params: [
          {
            title: 'Param 3-1',
            value: 'Value 3-1'
          }
        ]
      }
    ]
  },
  {
    title: 'Location 2',
    description: [
      {
        title: 'Loc 2 - Description 1',
        params: [
          {
            title: 'Param 1-1',
            value: 'Value 1-1'
          },
          {
            title: 'Param 1-2',
            value: 'Value 1-2'
          },
          {
            title: 'Param 1-3',
            value: 'Value 1-3'
          }
        ]
      },
      {
        title: 'Loc 2 - Description 2',
        params: [
          {
            title: 'Param 2-1',
            value: 'Value 2-1'
          }
        ]
      },
      {
        title: 'Loc 2 - Description 3',
        params: [
          {
            title: 'Param 3-1',
            value: 'Value 3-1'
          },
          {
            title: 'Param 3-2',
            value: 'Value 3-2'
          }
        ]
      }
    ]
  },
  {
    title: 'Location 3',
    description: [
      {
        title: 'Loc 3 - Description 1',
        params: [
          {
            title: 'Param 1-1',
            value: 'Value 1-1'
          }
        ]
      },
      {
        title: 'Loc 3 - Description 2',
        params: [
          {
            title: 'Param 2-1',
            value: 'Value 2-1'
          },
          {
            title: 'Param 2-2',
            value: 'Value 2-2'
          }
        ]
      },
      {
        title: 'Loc 3 - Description 3',
        params: [
          {
            title: 'Param 3-1',
            value: 'Value 3-1'
          },
          {
            title: 'Param 3-2',
            value: 'Value 3-2'
          },
          {
            title: 'Param 3-3',
            value: 'Value 3-3'
          }
        ]
      }
    ]
  },
  {
    title: 'Location 4',
    description: [
      {
        title: 'Loc 4 - Description 1',
        params: [
          {
            title: 'Param 1-1',
            value: 'Value 1-1'
          }
        ]
      },
      {
        title: 'Loc 4 - Description 2',
        params: [
          {
            title: 'Param 2-1',
            value: 'Value 2-1'
          },
          {
            title: 'Param 2-2',
            value: 'Value 2-2'
          },
          {
            title: 'Param 2-3',
            value: 'Value 2-3'
          }
        ]
      },
      {
        title: 'Loc 4 - Description 3',
        params: [
          {
            title: 'Param 3-1',
            value: 'Value 3-1'
          },
          {
            title: 'Param 3-2',
            value: 'Value 3-2'
          }
        ]
      }
    ]
  }
];

function initialize() {
  $('.menu-item').click(function () {
    $('.menu-item').removeClass('active');
    $(this).addClass('active');
    var index = $(this).index() - 2;
    showSidebar(locations[index]);
  });
}

function contextMenuHtml(title, description) {
  var html = '';
  html += '<div class="box-title">';
  html += '  <div>' + title + '</div>';
  html += '  <div class="box-closeButton">';
  html += '    <svg viewBox="0 0 24 24">';
  html += '      <path d="M22.2,4c0,0,0.5,0.6,0,1.1l-6.8,6.8l6.9,6.9c0.5,0.5,0,1.1,0,1.1L20,22.3c0,0-0.6,0.5-1.1,0L12,15.4l-6.9,6.9c-0.5,0.5-1.1,0-1.1,0L1.7,20c0,0-0.5-0.6,0-1.1L8.6,12L1.7,5.1C1.2,4.6,1.7,4,1.7,4L4,1.7c0,0,0.6-0.5,1.1,0L12,8.5l6.8-6.8c0.5-0.5,1.1,0,1.1,0L22.2,4z">';
  html += '      </path>';
  html += '    </svg>';
  html += '  </div>';
  html += '</div>';
  for (var i = 0; i < description.length; i++) {
    html += '<h3>' + description[i].title + '</h3>';
    for (var j = 0; j < description[i].params.length; j++) {
      html += '<div class="description-title col-xs-6">' + description[i].params[j].title + '</div>';
      html += '<div class="description-value col-xs-6">' + description[i].params[j].value + '</div>';
      html += '<div class="clearfix"></div>';
    }
  }

  return html;
}

function showSidebar(location) {
  $('#sidebar').addClass('active');
  $('#sidebar').html(contextMenuHtml(location.title, location.description));
  $('.box-closeButton').click(function () {
    $('#sidebar').removeClass('active');
  });
}

$(document).ready(function () {
  initialize();
});