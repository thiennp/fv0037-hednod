(function() {
		'use strict';

		angular
				.module('app', [
						'ng-sortable',
						'ui.router',
						'ngSanitize',
						'ngAnimate'
				])
				.config(routing)
				.controller('MainController', MainController)
				.controller('BillingController', BillingController)
				.controller('CustomersController', CustomersController)
				.directive('ngDynamicController', ngDynamicController);

		function ngDynamicController($compile, $parse) {
				return {
						restrict: 'A',
						terminal: true,
						priority: 100000,
						link: function(scope, elem) {
								var name = $parse(elem.attr('ng-dynamic-controller'))(scope);
								elem.removeAttr('ng-dynamic-controller');
								elem.attr('ng-controller', name);
								$compile(elem)(scope);
						}
				};
		}

		function routing($stateProvider, $urlRouterProvider) {

				$stateProvider
						.state('dashboard', {
								url: '/',
								templateUrl: 'dashboard.html'
						})
						.state('billing', {
								url: '/billing',
								templateUrl: 'billing.html',
								controller: BillingController,
								controllerAs: 'b'
						})
						.state('customers', {
								url: '/customers',
								templateUrl: 'customers.html',
								controller: CustomersController,
								controllerAs: 'c'
						})
						.state('about', {
								url: '/about',
								templateUrl: 'about.html',
								controller: BillingController,
								controllerAs: 'b'
						})

				$urlRouterProvider.otherwise('/');
		}

		function MainController($scope, $timeout) {
				var vm = this;

				vm.widgetConfig = {
						group: 'widget',
						animation: 150,
						handle: '.grabber',
						ghostClass: 'widget-ghost',
						sort: false,
						onStart: function(evt) {
								vm.adding = true;
						}
				};

				vm.widgetsConfig = {
						group: 'widget',
						animation: 150,
						handle: '.grabber',
						ghostClass: 'widget-ghost',
						onStart: function(evt) {
								vm.dragging = true;
						},
						onEnd: function(evt) {
								vm.dragging = false;
						},
						onAdd: function(evt) {
								console.log(evt.model.id);
								vm.added = evt.model.id;
								$timeout(function() {
										vm.adding = false;
										vm.added = false;
										$scope.$apply;
								}, 500);
						}
				};

				vm.added = false;
				vm.activeWidgets = [];
				vm.widgets = [{
						id: 1,
						name: 'Billing Linechart',
						controller: 'BillingController as b',
						template: 'billing.overview.html'
				}, {
						id: 2,
						name: 'Billing Donut Chart',
						controller: 'BillingController as b',
						template: 'billing.detail.html'
				}, {
						id: 3,
						name: 'Billing List',
						controller: 'BillingController as b',
						template: 'billing.list.html'
				}, {
						id: 4,
						name: 'Customer',
						controller: 'CustomersController as c',
						template: 'customers.overview.html'
				}, {
						id: 5,
						name: 'Customers',
						controller: 'CustomersController as c',
						template: 'customers.detail.html'
				}, {
						id: 6,
						name: 'Customer List',
						controller: 'CustomersController as c',
						template: 'customers.list.html'
				}];

				vm.findWidget = {};
				for (var i = 0, len = vm.widgets.length; i < len; i++) {
						vm.findWidget[vm.widgets[i].id] = vm.widgets[i];
				}

				vm.findWidgets = function(id) {
						return _.where(vm.widgets, {
								id: id
						});
				}

				vm.hasWidget = function(id) {
						return _.where(vm.activeWidgets, {
								id: id
						}).length > 0;
				}

				vm.toggleWidget = function(id) {
						var i = _.findIndex(vm.activeWidgets, {
								id: id
						});
						if (i > -1) {
								vm.activeWidgets.splice(i, 1);
						} else {
								var w = _.findWhere(vm.widgets, {
										id: id
								});
								vm.activeWidgets.push(w);
						}
				}

		}

		function BillingController() {
				var vm = this;

				vm.munnies = 5407;

		}

		function CustomersController() {
				var vm = this;

				vm.munnies = 3434;

		}

})();