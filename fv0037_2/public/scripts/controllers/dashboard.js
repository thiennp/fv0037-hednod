'use strict';

/**
 * @ngdoc function
 * @name minovateApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the minovateApp
 */
app
    .controller('DashboardCtrl', function($scope, $http) {
        $scope.init = function() {
            //console.log("Initialized");
            //console.log("Widgets id #2: " + JSON.stringify($scope.$parent.m.findWidgets(2)));
            //$scope.$parent.m.addWidget(1);
            $http({
                method: 'GET',
                url: '/api',
            }).then(function successCallback(response) {
                if(response.status == 201)
                {
                    for(var i=0;i<response.data.length;i++)
                    {
                        var obj = response.data[i];
                        $scope.$parent.m.addWidget(obj.id);
                    }
                }
                else {
                    console.error(response);
                }

            }, function errorCallback(response) {
                console.error(response);
            });
        }

        $scope.$on('itemRemoved', function(e, id) {

            $http({
                method: 'DELETE',
                url: '/api/' + id,
            }).then(function successCallback(response) {
                if(response.status == 201)
                {
                    //console.log("Item Removed: " + id);
                    console.log("Object Sucessfully Removed from MongoDB ");
                }
                else {
                    console.error(response);
                }

            }, function errorCallback(response) {
                console.error(response);
            });
        });
   	});
